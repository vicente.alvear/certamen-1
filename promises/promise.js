const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        // resolve('Correcto')
        reject('Incorrecto')
    }, 2000)
})

promise
    .then(response => console.log(response))
    .catch(error => console.log('Error: ' + error))